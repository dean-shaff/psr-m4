dnl @synopsis AC_PYTHON_DEVEL
dnl
dnl Checks for python and tries to get the include path to 'python.h'.
dnl It provides the $(PYTHON_CPPFLAGS) and $(PYTHON_LDFLAGS) output
dnl variable.
dnl
dnl Added check for Numpy - 2008/08/27 PBD
dnl
dnl @category InstalledPackages
dnl @author Sebastian Huber <sebastian-huber@web.de>
dnl @author Alan W. Irwin <irwin@beluga.phys.uvic.ca>
dnl @author Rafael Laboissiere <laboissiere@psy.mpg.de>
dnl @author Andrew Collier <colliera@nu.ac.za>
dnl @version 2004-07-14
dnl @license GPLWithACException

AC_DEFUN([AC_PYTHON_DEVEL],[

	AC_REQUIRE([AM_PATH_PYTHON])

	# Check for python include path
	AC_MSG_CHECKING([for python include path])
	python_path=`$PYTHON -c "from sysconfig import get_paths; print(get_paths().get('include'))"`
	AC_MSG_RESULT([$python_path])
	if test -z "$python_path" ; then
		AC_MSG_ERROR([cannot find python include path])
	fi


	# get numpy version
	AC_MSG_CHECKING([for numpy version])
	numpy_version=`$PYTHON -c "import numpy; print(numpy.version.full_version)"`
	AC_MSG_RESULT([$numpy_version])
	AC_SUBST([PYTHON_NUMPY_VERSION],["$numpy_version"])

	# Check for numpy include path
	AC_MSG_CHECKING([for numpy include path])
	numpy_include=`$PYTHON -c "import numpy; print(numpy.get_include())"`
	AC_MSG_RESULT([$numpy_include])
	if test -z "$numpy_include" ; then
		AC_MSG_ERROR([cannot find Numpy include path])
	fi

	# Output full include path
	AC_SUBST([PYTHON_CPPFLAGS],["-I$python_path -I$numpy_include"])

	# Check for python library path
	AC_MSG_CHECKING([for python library path])
	python_path=`$PYTHON -c "import os; from sysconfig import get_paths; print(os.path.dirname(get_paths().get('stdlib')))"`
	AC_MSG_RESULT([$python_path])

	if test -z "$python_path" ; then
		AC_MSG_ERROR([cannot find python library path])
	fi

	AC_MSG_CHECKING([for python library])
	python_major_version=`$PYTHON -c "import sys; print(sys.version_info[[0]])"`
	python_lib_name=`echo "python${python_major_version}"`
	AC_MSG_RESULT([lib$python_lib_name])
	AC_SUBST([PYTHON_LDFLAGS],["-L$python_path -l$python_lib_name"])

	python_site=`echo $python_path | sed "s/config/site-packages/"`
	AC_SUBST([PYTHON_SITE_PKG],[$python_site])

	# libraries which must be linked in when embedding
	AC_MSG_CHECKING([for python extra libraries])
	PYTHON_EXTRA_LIBS=`$PYTHON -c "import distutils.sysconfig; \
                conf = distutils.sysconfig.get_config_var; \
                print(conf('LOCALMODLIBS')+' '+conf('LIBS'))"`
	AC_MSG_RESULT([$PYTHON_EXTRA_LIBS])
	AC_SUBST(PYTHON_EXTRA_LIBS)
])
