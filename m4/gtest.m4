AC_DEFUN([GTEST],
[
  AC_PROVIDE([GTEST])

  AC_ARG_WITH([gtest-dir],
              [AS_HELP_STRING([--with-gtest-dir=DIR],
                             [GTEST is installed in DIR])])

  GTEST_CPPFLAGS="-pthread"
  GTEST_LDFLAGS=""

  if test x"$with_gtest_dir" != xno; then
    GTEST_DIR="$with_gtest_dir"
  fi

  if test x"$GTEST_DIR" != x; then

    AC_MSG_CHECKING([for googletest include path])

    gtest_include_dir="$GTEST_DIR/include"

    GTEST_CPPFLAGS="$GTEST_CPPFLAGS -I$gtest_include_dir"

    AC_MSG_RESULT([$gtest_include_dir])

    AC_MSG_CHECKING([for googletest library path])

    gtest_lib="$GTEST_DIR/lib"

    GTEST_LDFLAGS="$GTEST_LDFLAGS -L$gtest_lib"

    AC_MSG_RESULT([$gtest_lib])
  fi

  CPPFLAGS_before="$CPPFLAGS"
  CPPFLAGS="$CPPFLAGS $GTEST_CPPFLAGS"

  LDFLAGS_before="$LDFLAGS"
  LDFLAGS="$LDFLAGS $GTEST_LDFLAGS"

  AC_LANG_PUSH([C++])
  AC_CHECK_HEADERS([gtest/gtest.h], [have_gtest=yes], [have_gtest=no])
  if test x"$have_gtest" == xyes; then
    AC_CHECK_LIB([gtest_main], [main], [have_gtest=yes], [have_gtest=no])
    GTEST_LDFLAGS="$GTEST_LDFLAGS -lgtest_main -lgtest"
  fi
  AC_LANG_POP([C++])

  CPPFLAGS="$CPPFLAGS_before"
  LDFLAGS="$LDFLAGS_before"

  AC_SUBST(GTEST_CPPFLAGS)
  AC_SUBST(GTEST_LDFLAGS)
  AM_CONDITIONAL(HAVE_GTEST, [test x"$have_gtest" = xyes])

])
